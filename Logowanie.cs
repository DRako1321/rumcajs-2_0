﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System;
using Android.Views;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RUMCAJS_2_0
{
    [Activity(Label = "RUMCAJS", MainLauncher = true, Theme = "@style/Theme.Design.NoActionBar")]
    public class Logowanie : AppCompatActivity
    {
        public object MessageBox { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource

            SetContentView(layoutResID:  Resource.Layout.Logowanie);

            Button button = FindViewById<Button>(Resource.Id.PanelGl);
            button.Click += delegate
            {
                StartActivity(typeof(PanelGl));
            };
            Button button1 = FindViewById<Button>(Resource.Id.Rejestracja);
            button.Click += delegate
            {
                StartActivity(typeof(Rejestracja));
            };
        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}