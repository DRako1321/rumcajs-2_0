﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System;
using Android.Views;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RUMCAJS_2_0
{
    [Activity(Label = "Historia", Theme = "@style/Theme.Design.NoActionBar")]
    public class HistoriaTransakcji : AppCompatActivity
    {
        public object MessageBox { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource

            SetContentView(layoutResID:  Resource.Layout.HistoriaTransakcji);
            ImageButton button = FindViewById<ImageButton>(Resource.Id.PanelGl);
            button.Click += delegate
            {
                StartActivity(typeof(PanelGl));
            };
        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}