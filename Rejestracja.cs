﻿using Android.App;
using Android.OS;
using Android.Widget;
using System;
using System.Collections.Generic;
using RUMCAJS_2_0.Resources.Helper;
using RUMCAJS_2_0.Resources.Model;
namespace RUMCAJS_2_0
{
    [Activity(Label = "Rejestracja")]
    public class Rejestracja : Activity
    {
        ListView lstViewData;
        List<Klient> listSource = new List<Klient>();
        Database db;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource  
            SetContentView(layoutResID: Resource.Layout.Rejestracja);
            //Create Database  
            db = new Database();
            db.createDatabase();
            lstViewData = FindViewById<ListView>(Resource.Id.listView);
            var edtName = FindViewById<EditText>(Resource.Id.edtName);
            var edtDepart = FindViewById<EditText>(Resource.Id.edtDepart);
            var edtEmail = FindViewById<EditText>(Resource.Id.edtEmail);
            var btnAdd = FindViewById<Button>(Resource.Id.btnAdd);
            var btnEdit = FindViewById<Button>(Resource.Id.btnEdit);
            var btnRemove = FindViewById<Button>(Resource.Id.btnRemove);
            //Load Data  
            LoadData();
            //Event  
            btnAdd.Click += delegate
            {
                Klient klient = new Klient()
                {
                    Name = edtName.Text,
                    Department = edtDepart.Text,
                    Email = edtEmail.Text
                };
                db.insertIntoTable(klient);
                LoadData();
            };
            btnEdit.Click += delegate
            {
                Klient klient = new Klient()
                {
                    Id = int.Parse(edtName.Tag.ToString()),
                    Name = edtName.Text,
                    Department = edtDepart.Text,
                    Email = edtEmail.Text
                };
                db.updateTable(klient);
                LoadData();
            };
            btnRemove.Click += delegate
            {
                Klient klient = new Klient()
                {
                    Id = int.Parse(edtName.Tag.ToString()),
                    Name = edtName.Text,
                    Department = edtDepart.Text,
                    Email = edtEmail.Text
                };
                db.removeTable(klient);
                LoadData();
            };
            lstViewData.ItemClick += (s, e) =>
            {
                //Set Backround for selected item  
                for (int i = 0; i < lstViewData.Count; i++)
                {
                    if (e.Position == i)
                        lstViewData.GetChildAt(i).SetBackgroundColor(Android.Graphics.Color.Black);
                    else
                        lstViewData.GetChildAt(i).SetBackgroundColor(Android.Graphics.Color.Transparent);
                }
                //Binding Data  
                var txtName = e.View.FindViewById<TextView>(Resource.Id.txtView_Name);
                var txtDepart = e.View.FindViewById<TextView>(Resource.Id.txtView_Depart);
                var txtEmail = e.View.FindViewById<TextView>(Resource.Id.txtView_Email);
                edtEmail.Text = txtName.Text;
                edtName.Tag = e.Id;
                edtDepart.Text = txtDepart.Text;
                edtEmail.Text = txtEmail.Text;
            };
        }



        private void LoadData()
        {
            listSource = db.selectTable();
            var adapter = new ListViewAdapter(this, listSource);
            lstViewData.Adapter = adapter;
        }
    }
}